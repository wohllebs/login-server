from http import HTTPStatus
from flask import Flask, abort, request, send_from_directory, make_response, render_template
from werkzeug.datastructures import WWWAuthenticate
import flask
from login_form import LoginForm
from json import dumps, loads
from base64 import b64decode
import sys
import apsw
from apsw import Error
from pygments import highlight
from pygments.lexers import SqlLexer
from pygments.formatters import HtmlFormatter
from pygments.filters import NameHighlightFilter, KeywordCaseFilter
from pygments import token;
from threading import local
from markupsafe import escape

import bcrypt, uuid, time
from datetime import datetime

tls = local()
cssData = HtmlFormatter(nowrap=True).get_style_defs('.highlight')
conn = None
passwords = None

# Set up app
app = Flask(__name__)
# The secret key enables storing encrypted session data in a cookie (make a secure random key for this!)
app.secret_key = 'mY s3kritz'

# Add a login manager to the app
import flask_login
from flask_login import login_required, login_user, logout_user
login_manager = flask_login.LoginManager()
login_manager.init_app(app)
login_manager.login_view = "login"




# Class to store user info
# UserMixin provides us with an `id` field and the necessary
# methods (`is_authenticated`, `is_active`, `is_anonymous` and `get_id()`)
class User(flask_login.UserMixin):
    pass


# This method is called whenever the login manager needs to get
# the User object for a given user id
@login_manager.user_loader
def user_loader(user_id):


    c = passwords.execute("SELECT * from passwords WHERE user GLOB ?",(user_id,))
    res = c.fetchall()
    if not res:
        return

    user = User()
    user.id = res[0][1]
    return user


# This method is called to get a User object based on a request,
# for example, if using an api key or authentication token rather
# than getting the user name the standard way (from the session cookie)
@login_manager.request_loader
def request_loader(request):
    # Even though this HTTP header is primarily used for *authentication*
    # rather than *authorization*, it's still called "Authorization".
    auth = request.headers.get('Authorization')

    # If there is not Authorization header, do nothing, and the login
    # manager will deal with it (i.e., by redirecting to a login page)
    if not auth:
        return

    (auth_scheme, auth_params) = auth.split(maxsplit=1)
    auth_scheme = auth_scheme.casefold()
    if auth_scheme == 'basic':  # Basic auth has username:password in base64
        (username,password) = b64decode(auth_params.encode(errors='ignore')).decode(errors='ignore').split(':', maxsplit=1)
        print(f'Basic auth: {uid}:{passwd}')
        stmt = "SELECT * FROM passwords WHERE user GLOB ?"
        c = passwords.execute(stmt, (username,))
        res = c.fetchall()
        if res and bcrypt.checkpw(password, res[0][2]):
            return user_loader(res[0][1])
    elif auth_scheme == 'bearer': # Bearer auth contains an access token;
        # an 'access token' is a unique string that both identifies
        # and authenticates a user, so no username is provided (unless
        # you encode it in the token – see JWT (JSON Web Token), which
        # encodes credentials and (possibly) authorization info)
        print(f'Bearer auth: {auth_params}')
        c = passwords.execute("SELECT * from passwords WHERE token GLOB ?",(auth_params,))
        res = c.fetchall()
        if res:
            return user_loader(res[0][1])
    # For other authentication schemes, see
    # https://developer.mozilla.org/en-US/docs/Web/HTTP/Authentication

    # If we failed to find a valid Authorized header or valid credentials, fail
    # with "401 Unauthorized" and a list of valid authentication schemes
    # (The presence of the Authorized header probably means we're talking to
    # a program and not a user in a browser, so we should send a proper
    # error message rather than redirect to the login page.)
    # (If an authenticated user doesn't have authorization to view a page,
    # Flask will send a "403 Forbidden" response, so think of
    # "Unauthorized" as "Unauthenticated" and "Forbidden" as "Unauthorized")
    abort(HTTPStatus.UNAUTHORIZED, www_authenticate = WWWAuthenticate('Basic realm=inf226, Bearer'))

def pygmentize(text):
    if not hasattr(tls, 'formatter'):
        tls.formatter = HtmlFormatter(nowrap = True)
    if not hasattr(tls, 'lexer'):
        tls.lexer = SqlLexer()
        tls.lexer.add_filter(NameHighlightFilter(names=['GLOB'], tokentype=token.Keyword))
        tls.lexer.add_filter(NameHighlightFilter(names=['text'], tokentype=token.Name))
        tls.lexer.add_filter(KeywordCaseFilter(case='upper'))
    return f'<span class="highlight">{highlight(text, tls.lexer, tls.formatter)}</span>'

@app.route('/favicon.ico')
def favicon_ico():
    return send_from_directory(app.root_path, 'favicon.ico', mimetype='image/vnd.microsoft.icon')

@app.route('/favicon.png')
def favicon_png():
    return send_from_directory(app.root_path, 'favicon.png', mimetype='image/png')



@app.route('/')
@app.route('/index.html')
@login_required
def index_html():
    return send_from_directory(app.root_path,
                        'index.html', mimetype='text/html')

@app.route('/login', methods=['GET', 'POST'])
def login():
    form = LoginForm()
    if form.is_submitted():
        print(f'Received form: {"invalid" if not form.validate() else "valid"} {form.form_errors} {form.errors}')
        print(request.form)
    if form.validate_on_submit():
        username = form.username.data
        password = form.password.data
        stmt = "SELECT * FROM passwords WHERE user GLOB ?"
        c = passwords.execute(stmt, (username,))
        res = c.fetchall()
        if res and bcrypt.checkpw(password, res[0][2]):
            user = user_loader(username)
            
            # automatically sets logged in session cookie
            login_user(user)
            flask.session['Username'] = username
            
            #flask.flash('Logged in successfully.')

            next = flask.request.args.get('next')
    
            # is_safe_url should check if the url is safe for redirects.
            # See http://flask.pocoo.org/snippets/62/ for an example.
            if False and not is_safe_url(next):
                return flask.abort(400)

            return flask.redirect(next or flask.url_for('index_html'))
        else:
       	    flask.flash("Wrong password or user doesn't exist!")
    return render_template('./login.html', form=form)

@app.route('/signup',  methods=['GET', 'POST'])
def signup():
	form = LoginForm()
	if form.is_submitted():
		print(f'Received form: {"invalid" if not form.validate() else "valid"} {form.form_errors} {form.errors}')
		print(request.form)
	if form.validate_on_submit():
		username = form.username.data
		password = form.password.data
		c = passwords.cursor()
		res = c.execute("SELECT * FROM passwords WHERE user GLOB ?", (username,)).fetchall()
		if not res:
			flask.flash("User already exists")
			return render_template('./login.html', form=form)
		hashed = bcrypt.hashpw(password, bcrypt.gensalt())

		c.execute(f"INSERT INTO passwords ( user, hash, token) values ( ?, ?, 'tiktok')",(username, hashed))
		return flask.redirect(next or '/login')
	return render_template('./login.html', form=form)
		

@app.route('/logout', methods=['GET', 'POST'])
def logout():
    flask.session.pop("Username")
    logout_user()
    return flask.redirect('/login')

@app.get('/search')
def search():
    query = request.args.get('q') or request.form.get('q') or '*'
    stmt = "SELECT * FROM messages WHERE message GLOB ? AND (sender GLOB ? OR Recipient GLOB ?)"
    #result = f"Query: {pygmentize(stmt)}\n"
    try:
        c = conn.execute(stmt, (query,    flask.session['Username'], flask.session['Username'] ))
        rows = c.fetchall()
        result = ''
        for row in rows:
            result += f'   {datetime.fromtimestamp(row[1])} {row[2]} to {row[3]}\n   {row[4]}\n'
        c.close()
        return result
    except Error as e:
        return (f'{result}ERROR: {e}', 500)

@app.route('/send', methods=['POST','GET'])
def send():
    print(flask.session['Username'])
    try:
        recipient = request.args.get('sender') or request.form.get('sender')
        message = request.args.get('message') or request.args.get('message')
        if not recipient or not message:
            return f'ERROR: missing sender or message'
        res = passwords.execute("SELECT * FROM passwords WHERE user GLOB ?", (recipient,)).fetchall()
        if not res:
            return "ERROR: Recipient does not exist!"
        stmt = "INSERT INTO messages (time, sender,recipient, message) values (?,?,?, ?);"
        #result = f"Query: {pygmentize(stmt)}\n"
        t0 = time.time()
        conn.execute(stmt,(t0,flask.session['Username'],recipient, message))
        return f"   {datetime.fromtimestamp(t0)} {flask.session['Username']} to {recipient}\n   {message}"
    except Error as e:
        return f'ERROR: {e}'

@app.get('/announcements')
def announcements():
    try:
        stmt = f"SELECT author,text FROM announcements;"
        c = conn.execute(stmt)
        anns = []
        for row in c:
            anns.append({'sender':escape(row[0]), 'message':escape(row[1])})
        return {'data':anns}
    except Error as e:
        return {'error': f'{e}'}


@app.get('/highlight.css')
def highlightStyle():
    resp = make_response(cssData)
    resp.content_type = 'text/css'
    return resp

try:
    conn = apsw.Connection('./tiny.db')
    c = conn.cursor()
    c.execute('''CREATE TABLE IF NOT EXISTS messages (
        id integer PRIMARY KEY AUTOINCREMENT, 
        time INTEGER NOT NULL,
        sender TEXT NOT NULL,
        recipient TEXT NOT NULL,
        message TEXT NOT NULL);''')
    c.execute('''CREATE TABLE IF NOT EXISTS announcements (
        id integer PRIMARY KEY, 
        author TEXT NOT NULL,
        text TEXT NOT NULL);''')
    passwords = apsw.Connection('./passwords.db')
    c = passwords.cursor()
    c.execute('''CREATE TABLE IF NOT EXISTS passwords (
        id integer PRIMARY KEY AUTOINCREMENT,
        user TEXT NOT NULL,
        hash BLOB NOT NULL,
        token TEXT);''')
    c.execute(f"INSERT INTO passwords ( user, hash, token) values ( 'alice', '{bcrypt.hashpw('password123', bcrypt.gensalt())}', 'tiktok')")
    c.execute(f"INSERT INTO passwords ( user, hash) values ( 'bob', '{bcrypt.hashpw('bananas', bcrypt.gensalt())}')")
except Error as e:
    print(e)
    sys.exit(1)
