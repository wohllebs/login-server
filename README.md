

## To use
You need to install these Python packages first – [Flask](https://flask.palletsprojects.com/en/2.2.x/), [APSW](https://rogerbinns.github.io/apsw/), [Pygments](https://pygments.org/),[Bcrypt](https://pypi.org/project/bcrypt/):

```shell
pip install flask
pip install apsw
pip install pygments
pip install bcrypt
```

## Start the Web Server
Use the `flask` command to start the web server:

```shell
$ cd login-server
$ flask run
```

If the `flask` command doesn't exist, you can start use `python -m flask run` instead.

```shell
$ python -m flask run
 * Debug mode: off
WARNING: This is a development server. Do not use it in a production deployment. Use a production WSGI server instead.
 * Running on http://127.0.0.1:5000
Press CTRL+C to quit
```

Assuming it works, you should find the web server in your browser at http://localhost:5000/ 

(For server apps that aren't called `app.py`, you can add the `--app` option: `flask --app hello.py run`)

**WARNING:** *Don't expose this example server to the internet – it's horribly insecure!*

## Documentation
Changed the SQL Querys to prepared statements to prevent SQL Injection<br>
Storing Passowrd hashes with salt to protect them in case of a leak<br>
removed coffee routines because they were useless<br>
introduced a logout button that terminates the session<br> 
added a signup routine <br>
restricted User to only be able to send messages from themself and only see messages that they send or that were addressed to them <br>


## TO-DO
protection against XSS<br>
better messaging functionality <br>

